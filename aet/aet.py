
import laet.fem as fem
from numpy import sqrt
from numpy.random import randn

__all__ = ['J',
           'J_L1_TV',
           'J_L2_TV',
           'J_L2_L2',
           'J_L2_H1',
           'simulate_data',
           'potential',
           'power_density',
           'createR',
           'createR_TV',
           'createR_H1',
           'createAb',
           'createAb_L1',
           'createAb_L2']

def J_L1_TV(s,f,z,V,Vs,GradSolver,P):
    """Optimization functional
    """
    tmpV = fem.Vector(V)
    tmpVs = fem.Vector(Vs)

    S = fem.fem.Function(Vs,s)
    Ks = fem.StiffnessMatrixV(V,w=S)
    pdeSolver = fem.Solver(Ks)

    val = 0
    for i in range(len(f)):
        pdeSolver.solve(tmpV,f[i])
        P.transpmult(tmpV,tmpVs)
        u = tmpVs.copy()

        dx,dy = GradSolver(u)
        du2 = dx*dx + dy*dy
        h = s*du2

        d = h - z[i]
        val += fem.normp(d,Vs,1)

    reg_val = fem.TVseminorm(s,Vs)
    return val, reg_val

def J_L2_TV(s,f,z,V,Vs,GradSolver,P):
    """Optimization functional
    """
    tmpV = fem.Vector(V)
    tmpVs = fem.Vector(Vs)

    S = fem.fem.Function(Vs,s)
    Ks = fem.StiffnessMatrixV(V,w=S)
    M = fem.MassMatrixVs(Vs)
    pdeSolver = fem.Solver(Ks)

    val = 0
    for i in range(len(f)):
        pdeSolver.solve(tmpV,f[i])
        P.transpmult(tmpV,tmpVs)
        u = tmpVs.copy()

        dx,dy = GradSolver(u)
        du2 = dx*dx + dy*dy
        h = s*du2

        d = h - z[i]
        val += fem.norm2(d,M)

    reg_val = fem.TVseminorm(s,Vs)
    return val, reg_val

def J_L2_L2(s,f,z,V,Vs,GradSolver,P):
    """Optimization functional
    """
    tmpV = fem.Vector(V)
    tmpVs = fem.Vector(Vs)

    S = fem.fem.Function(Vs,s)
    Ks = fem.StiffnessMatrixV(V,w=S)
    M = fem.MassMatrixVs(Vs)
    pdeSolver = fem.Solver(Ks)

    val = 0
    for i in range(len(f)):
        pdeSolver.solve(tmpV,f[i])
        P.transpmult(tmpV,tmpVs)
        u = tmpVs.copy()

        dx,dy = GradSolver(u)
        du2 = dx*dx + dy*dy
        h = s*du2

        d = h - z[i]
        val += fem.norm2(d,M)

    reg_val = fem.norm2(s,M)
    return val, reg_val

def J_L2_H1(s,f,z,V,Vs,GradSolver,P):
    """Optimization functional
    """
    tmpV = fem.Vector(V)
    tmpVs = fem.Vector(Vs)

    S = fem.fem.Function(Vs,s)
    Ks = fem.StiffnessMatrixV(V,w=S)
    M = fem.MassMatrixVs(Vs)
    K = fem.StiffnessMatrixVs(Vs)
    pdeSolver = fem.Solver(Ks)

    val = 0
    for i in range(len(f)):
        pdeSolver.solve(tmpV,f[i])
        P.transpmult(tmpV,tmpVs)
        u = tmpVs.copy()

        dx,dy = GradSolver(u)
        du2 = dx*dx + dy*dy
        h = s*du2

        d = h - z[i]
        val += fem.norm2(d,M)

    reg_val = fem.norm2(s,M) + fem.norm2(s,K)
    return val, reg_val

def J(s,f,z,V,Vs,GradSolver,P):
    """ Backwards compatibility
    """
    return J_L1_TV(s,f,z,V,Vs,GradSolver,P)

def GaussianRandomVector(V):
    r = fem.Vector(V)
    r[:] = randn(r.size())
    return r

def simulate_data(sp,F,U,V,Vs,delta=0,Ubn=None,Mbdry=None):
    """Simulated data
    Computes
        I = <f, gp - g>
    where gp = up|bdry and g = u|bdry,
        -div(s*grad(u)) = 0
           s*grad(u).nu = f
    and
        -div(sp*grad(up)) = 0
           sp*grad(up).nu = f
    Parameters:
        sp, <vector>, perturbed conductivity, sp = s*(1+eta*p)
        F,  [<vector>,...], the Neumann boundary conditions
        U,  [<vector>,...], the solutions u for non-perturbed conductivity
        V,  FunctionSpace, the FEM space for H1-diamond
        Vs, FunctionSpace, the FEM space for H1
        delta,  <scalar>, relative noise level (%) in data, (default: 0)
        Ubn, [<scalar>,...], the boundary norm of U. Only needed for delta =/= 0.
        Mbdry, <matrix>, mass matrix on the boundary of V. Only needed for delta =/= 0.
    """
    # Init
    Sp = fem.fem.Function(Vs,sp)
    Ksp = fem.StiffnessMatrixV(V,w=Sp)
    P = fem.VsVInjector(Vs, V)
    pde = fem.Solver(Ksp)
    tmpV = fem.Vector(V)
    tmpVs = fem.Vector(Vs)
    
    # Noise init
    if delta != 0:
        assert Ubn is not None, "Boundary norm of U must be supplied when delta > 0"
        assert Mbdry is not None, "Boundary mass matrix must be supplied when delta > 0"

    # Loop
    I = []
    for i in range(len(F)):
        # Compute data
        f = F[i]
        u = U[i]
        pde.solve(tmpV, F[i])
        P.transpmult(tmpV,tmpVs)
        up = tmpVs.copy()
       
        P.transpmult(f, tmpVs)
        I.append(tmpVs.inner(up - u))

        # Add noise
        if delta != 0:
            r = GaussianRandomVector(V)
            Mbdry.mult(r,tmpV)
            rn = sqrt(tmpV.inner(r))
            noise = delta*Ubn[i]*r/rn
            I[i] += f.inner(noise)
    return I

def potential(s,F,V,Vs):
    """ Potential solver
    Computes the solution u[i] of the pde
    
        -div(s*grad(u)) = 0
           s*grad(u).nu = f[i]
           
    Parameters:
        s,  <vector>, the conductivity sigma
        F,  [<vector>,...], the Neumann boundary conditions
        V,  FunctionSpace, the FEM space for H1-diamond
        Vs, FunctionSpace, the FEM space for H1
        
    Note: This is not efficient if you want to solve the equation 
          many times, but for once or twice it is good enough.
    """
    S = fem.fem.Function(Vs,s)
    Ks = fem.StiffnessMatrixV(V,w=S)
    P = fem.VsVInjector(Vs,V)
    pde = fem.Solver(Ks)
    U = []
    tmpV = fem.Vector(V)
    tmpVs = fem.Vector(Vs)
    for f in F:
        pde.solve(tmpV,f)
        P.transpmult(tmpV,tmpVs)
        U.append(tmpVs.copy())
    return U

def power_density(*args):
    """ Power density solver
    Computes
    
        H[i] = s*|grad(u[i])|^2
        
    where u[i] solves
    
        -div(s*grad(u)) = 0
           s*grad(u).nu = f[i]
    
    Input syntaxes:
        power_density(s,U,GradSolver)
        power_density(s,F,V,Vs,GradSolver)
    
    Parameters:
        s,  <vector>, the conductivity sigma
        U,  [<vector>,...], the potential solutions
        F,  [<vector>,...], the Neumann boundary conditions
        V,  FunctionSpace, the FEM space for H1-diamond
        Vs, FunctionSpace, the FEM space for H1
        GradSolver, method, computes the gradient (dx,dy) of
                    the input vector as a function in Vs.
                    
    Note#1: This is not efficient if you want to solve the equation 
            many times, but for once or twice it is good enough.
            
    Note#2: Interpolation onto Vs is used in place of 
            projection for improved speed.
    """
    if len(args) == 3:
        # power_density(s,U,GradSolver):
        s,U,GradSolver = args
    if len(args) == 5:
        # power_density(s,F,V,Vs,GradSolver):
        s,F,V,Vs,GradSolver = args
        U = potential(s,F,V,Vs)
    dU = [GradSolver(u) for u in U]
    dU2 = [dx*dx + dy*dy for (dx,dy) in dU]
    H = [s*du2 for du2 in dU2]
    return H
        
def createR_TV(sv,Vs,GradSolver,eps):
    """Creates the regularization matrix for Total Variation regularization
    """
    tmpVs = fem.Vector(Vs)
    dx,dy = GradSolver(sv)
    tmpVs[:] = 1.0/sqrt(dx*dx + dy*dy + eps**2)
    w0 = fem.fem.Function(Vs,tmpVs.copy())
    R = fem.StiffnessMatrixVs(Vs,w=w0)
    return R
    
def createR_H1(Vs):
    """Creates the regularization matrix for H1 regularization
    """
    R = fem.MassMatrixVs(Vs) + fem.StiffnessMatrixVs(Vs)
    return R
    
def createR(sv,Vs,GradSolver,eps):
    """ Backwards compatibility
    """
    return createR_TV(sv,Vs,GradSolver,eps)
    
def createAb_L1(s,f,z,V,Vs,GradSolver,massSolver,P,eps):
    """Creates the linear operator A and rhs b
    ===========================================================
    Parameters:
    -----------------------------------------------------------
        sv, the fixed sigma as vector
        fv, list [] of boundary conditions as rhs vectors
            for the pde problem
        z,  list [] of measurements data, H = s*|grad(u)|^2
        V,  H1D function space approximation, Vs x R
        Vs, H1 function space approximation
        GradSolver, gradient solver for Vs which computes
                    dx and dy for the inputed function vector
        massSolver, solver computing the action of the inverse
                    of the mass matrix for Vs on the inputed
                    function vector
        P,  Vs-to-V injector matrix, essentially an identity
            matrix with an extra zero-row at the top, (n+1) x n.
            P^T is the projection V-to-Vs.
        eps,    smoothing parameter
    ===========================================================
    """
    tmpV = fem.Vector(V)
    tmpVs = fem.Vector(Vs)
    tmpVs2 = fem.Vector(Vs)
    tmpVs3 = fem.Vector(Vs)
    tmpVs4 = fem.Vector(Vs)

    S = fem.fem.Function(Vs,s)
    Ks = fem.StiffnessMatrixV(V,w=S)
    pdeSolver = fem.Solver(Ks)

    # initialize
    d,Mw,Mu,Lu,Wsut = [],[],[],[],[]
    for i in range(len(f)):
        # Compute u
        pdeSolver.solve(tmpV,f[i])
        P.transpmult(tmpV,tmpVs)
        u = tmpVs.copy()

        # Compute h
        dx,dy = GradSolver(u)
        du2 = dx*dx + dy*dy
        h = s*du2
        U = fem.fem.Function(Vs,u.copy())
        DU2 = fem.fem.Function(Vs,du2.copy())

        # Weight
        d.append(z[i] - h)
        tmpVs[:] = 1.0/sqrt(d[i]*d[i] + eps**2)
        W = fem.fem.Function(Vs,tmpVs.copy())

        # Matrices
        Mw.append(fem.MassMatrixVs(Vs,w=W))
        Mu.append(fem.MassMatrixVs(Vs,w=DU2))
        Lu.append(fem.MixedMatrixVs(Vs,U))
        Wsut.append(fem.MixedMatrixVs(Vs,U,w=S))

    # Construct linear operator
    def A(x):
        tmpVs = x.copy()
        tmpVs4[:] = 0                           #       tmpVs4 = vec(0)
        for i in range(len(f)):
            tmpVs2[:] = x.get_local().copy()
            Lu[i].mult(tmpVs2,tmpVs)            #    Lu*tmpVs2 = tmpVs
            P.mult(tmpVs,tmpV)                  #      P*tmpVs = tmpV
            pdeSolver.solve(tmpV,tmpV)          #         tmpV = inv(Kw)*tmpV
            P.transpmult(tmpV,tmpVs)            #     P^T*tmpV = tmpVs
            Wsut[i].transpmult(tmpVs,tmpVs3)    # Wsut^T*tmpVs = tmpVs3
            Mu[i].mult(tmpVs2,tmpVs)            #    Mu*tmpVs2 = tmpVs
            tmpVs.axpy(-2,tmpVs3)               #        tmpVs = tmpVs - 2*tmpVs3
            massSolver.solve(tmpVs,tmpVs)       #        tmpVs = inv(M)*tmpVs
            Mw[i].mult(tmpVs,tmpVs2)            #     Mw*tmpVs = tmpVs2
            massSolver.solve(tmpVs,tmpVs2)      #        tmpVs = inv(M)*tmpVs2
            Wsut[i].mult(tmpVs,tmpVs2)          #   Wsut*tmpVs = tmpVs2
            P.mult(tmpVs2,tmpV)                 #     P*tmpVs2 = tmpV
            pdeSolver.solve(tmpV,tmpV)          #         tmpV = inv(Kw)*tmpV
            P.transpmult(tmpV,tmpVs2)           #     P^T*tmpV = tmpVs2
            Lu[i].transpmult(tmpVs2,tmpVs3)     #   Lu^T*tmpVs2 = tmpVs3
            Mu[i].mult(tmpVs,tmpVs2)            #     Mu*tmpVs = tmpVs2
            tmpVs2.axpy(-2,tmpVs3)              #       tmpVs2 = tmpVs2 - 2*tmpVs3
            tmpVs4.axpy(1,tmpVs2)               #       tmpVs4 = tmpVs4 + tmpVs2
        return tmpVs4.copy()                  # return tmpVs

    # Matrix like methods and properties
    A.shape = (len(tmpVs),len(tmpVs))
    def mult(v,w):
        w[:] = A(v).get_local()
    A.mult = mult #lambda v,w: w.set_local(A(v).get_local())

    # RHS data
    tmpVs4[:] = 0                               #       tmpVs4 = vec(0)
    for i in range(len(f)):
        tmpVs[:] = 0
        tmpVs.axpy(1,d[i])                      #        tmpVs = tmpVs + d[i]
        Mw[i].mult(tmpVs,tmpVs2)                #     Mw*tmpVs = tmpVs2
        massSolver.solve(tmpVs,tmpVs2)          #         tmpVs = inv(M)*tmpVs2
        Wsut[i].mult(tmpVs,tmpVs2)              #   Wsut*tmpVs = tmpVs2
        P.mult(tmpVs2,tmpV)                     #     P*tmpVs2 = tmpV
        pdeSolver.solve(tmpV,tmpV)              #         tmpV = inv(Kw)*tmpV
        P.transpmult(tmpV,tmpVs2)               #     P^T*tmpV = tmpVs2
        Lu[i].transpmult(tmpVs2,tmpVs3)         #  Lu^T*tmpVs2 = tmpVs3
        Mu[i].mult(tmpVs,tmpVs2)                #     Mu*tmpVs = tmpVs2
        tmpVs2.axpy(-2,tmpVs3)                  #       tmpVs2 = tmpVs2 - 2*tmpVs3
        tmpVs4.axpy(1,tmpVs2)                   #       tmpVs4 = tmpVs4 + tmpVs2
    b = tmpVs4.copy()

    return A, b

def createAb(s,f,z,V,Vs,GradSolver,massSolver,P,eps):
    """ Backwards compatibility
    """
    return createAb_L1(s,f,z,V,Vs,GradSolver,massSolver,P,eps)

def createAb_L2(s,f,z,V,Vs,GradSolver,massSolver,P,eps):
    """Creates the linear operator A and rhs b
    ===========================================================
    Parameters:
    -----------------------------------------------------------
        sv, the fixed sigma as vector
        fv, list [] of boundary conditions as rhs vectors
            for the pde problem
        z,  list [] of measurements data, H = s*|grad(u)|^2
        V,  H1D function space approximation, Vs x R
        Vs, H1 function space approximation
        GradSolver, gradient solver for Vs which computes
                    dx and dy for the inputed function vector
        massSolver, solver computing the action of the inverse
                    of the mass matrix for Vs on the inputed
                    function vector
        P,  Vs-to-V injector matrix, essentially an identity
            matrix with an extra zero-row at the top, (n+1) x n.
            P^T is the projection V-to-Vs.
        eps,    smoothing parameter
    ===========================================================
    """
    tmpV = fem.Vector(V)
    tmpVs = fem.Vector(Vs)
    tmpVs2 = fem.Vector(Vs)
    tmpVs3 = fem.Vector(Vs)
    tmpVs4 = fem.Vector(Vs)

    S = fem.fem.Function(Vs,s)
    Ks = fem.StiffnessMatrixV(V,w=S)
    pdeSolver = fem.Solver(Ks)

    # initialize
    d,Mu,Lu,Wsut = [],[],[],[]
    for i in range(len(f)):
        # Compute u
        pdeSolver.solve(tmpV,f[i])
        P.transpmult(tmpV,tmpVs)
        u = tmpVs.copy()

        # Compute h
        dx,dy = GradSolver(u)
        du2 = dx*dx + dy*dy
        h = s*du2
        U = fem.fem.Function(Vs,u.copy())
        DU2 = fem.fem.Function(Vs,du2.copy())

        # Weight
        d.append(z[i] - h)

        # Matrices
        Mu.append(fem.MassMatrixVs(Vs,w=DU2))
        Lu.append(fem.MixedMatrixVs(Vs,U))
        Wsut.append(fem.MixedMatrixVs(Vs,U,w=S))

    # Construct linear operator
    def A(x):
        tmpVs = x.copy()
        tmpVs4[:] = 0                           #       tmpVs4 = vec(0)
        for i in range(len(f)):
            # --- Computes H'(s)[X]
            tmpVs2[:] = x.get_local().copy()
            Lu[i].mult(tmpVs2,tmpVs)            #    Lu*tmpVs2 = tmpVs
            P.mult(tmpVs,tmpV)                  #      P*tmpVs = tmpV
            pdeSolver.solve(tmpV,tmpV)          #         tmpV = inv(Kw)*tmpV
            P.transpmult(tmpV,tmpVs)            #     P^T*tmpV = tmpVs
            Wsut[i].transpmult(tmpVs,tmpVs3)    # Wsut^T*tmpVs = tmpVs3
            Mu[i].mult(tmpVs2,tmpVs)            #    Mu*tmpVs2 = tmpVs
            tmpVs.axpy(-2,tmpVs3)               #        tmpVs = tmpVs - 2*tmpVs3
            massSolver.solve(tmpVs,tmpVs)       #        tmpVs = inv(M)*tmpVs
            # --- 
            # Here two mass matrices cancelled
            # --- Computes H'(s)*[MX]
            Wsut[i].mult(tmpVs,tmpVs2)          #   Wsut*tmpVs = tmpVs2
            P.mult(tmpVs2,tmpV)                 #     P*tmpVs2 = tmpV
            pdeSolver.solve(tmpV,tmpV)          #         tmpV = inv(Kw)*tmpV
            P.transpmult(tmpV,tmpVs2)           #     P^T*tmpV = tmpVs2
            Lu[i].transpmult(tmpVs2,tmpVs3)     #   Lu^T*tmpVs2 = tmpVs3
            Mu[i].mult(tmpVs,tmpVs2)            #     Mu*tmpVs = tmpVs2
            tmpVs2.axpy(-2,tmpVs3)              #       tmpVs2 = tmpVs2 - 2*tmpVs3
            tmpVs4.axpy(1,tmpVs2)               #       tmpVs4 = tmpVs4 + tmpVs2
            # ---
        return tmpVs4.copy()                  # return tmpVs

    # Matrix like methods and properties
    A.shape = (len(tmpVs),len(tmpVs))
    def mult(v,w):
        w[:] = A(v).get_local()
    A.mult = mult #lambda v,w: w.set_local(A(v).get_local())

    # RHS data
    tmpVs4[:] = 0                               #       tmpVs4 = vec(0)
    for i in range(len(f)):
        tmpVs[:] = 0
        tmpVs.axpy(1,d[i])                      #        tmpVs = tmpVs + d[i]
        # Two mass matrices cancelled
        Wsut[i].mult(tmpVs,tmpVs2)              #   Wsut*tmpVs = tmpVs2
        P.mult(tmpVs2,tmpV)                     #     P*tmpVs2 = tmpV
        pdeSolver.solve(tmpV,tmpV)              #         tmpV = inv(Kw)*tmpV
        P.transpmult(tmpV,tmpVs2)               #     P^T*tmpV = tmpVs2
        Lu[i].transpmult(tmpVs2,tmpVs3)         #  Lu^T*tmpVs2 = tmpVs3
        Mu[i].mult(tmpVs,tmpVs2)                #     Mu*tmpVs = tmpVs2
        tmpVs2.axpy(-2,tmpVs3)                  #       tmpVs2 = tmpVs2 - 2*tmpVs3
        tmpVs4.axpy(1,tmpVs2)                   #       tmpVs4 = tmpVs4 + tmpVs2
    b = tmpVs4.copy()

    return A, b


