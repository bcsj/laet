
from sksparse.cholmod import cholesky as chmodchol
from dolfin import __version__ as DOLFINversion

from dolfin import as_backend_type
from dolfin import Matrix, PETScMatrix
from dolfin import Vector


if DOLFINversion == '2017.2.0':
    from dolfin import mpi_comm_self
else:
    from dolfin import MPI

from scipy.sparse import csr_matrix
from petsc4py import PETSc

__all__ = ['cholesky', 
           'transpose',
           'zeroVector',
           'eye',
           'ToScipy',
           'FromScipy',
           'Anorm',
           'cg',
           'cgp']

def cholesky(M,beta=0):
    """Cholesky factorization for DOLFIN matrices
    Parameters:
        M,      matrix
        beta,   regularization parameter
    Computes the Cholesky factorization
                 P*M*P' = L*L'
        P*(M+beta*I)*P' = L*L'
    Returns: 
        L,  Cholesky factor, lower triangular
        P,  Sparsity permutation matrix
    """
    S = ToScipy(M)
    factor = chmodchol(S.tocsc(),beta=beta)
    L = FromScipy(factor.L())

    P = factor.P()
    li = list(range(len(P)+1))
    n = L.size(0)
    P = PETSc.Mat().createAIJWithArrays((n,n),(li,P,[1 for _ in li[1:]]))

    return L, Matrix(PETScMatrix(P))

def transpose(M):
    """Transpose for matrix of DOLFIN type
    """
    M_ = as_backend_type(M).mat()
    Q = M_.copy()
    Q.transpose()
    return Matrix(PETScMatrix(Q))

def zeroVector(n):
    """Zero-vector of DOLFIN type
    """
    if DOLFINversion == '2017.2.0':
        return Vector(mpi_comm_self(),n)
    else:
        return Vector(MPI.comm_self,n)
    # use MPI.comm_self() in 2018.1.0 and later

def eye(n):
    """Identity Matrix of DOLFIN type
    """
    li = list(range(n+1))
    I = PETSc.Mat().createAIJWithArrays((n,n),(li,li[:-1],[1 for _ in li[1:]]))
    return Matrix(PETScMatrix(I))

def ToScipy(M):
    """Creates a sparse Scipy matrix from a DOLFIN type matrix
    """
    M_ = as_backend_type(M).mat()
    return csr_matrix(M_.getValuesCSR()[::-1],shape=M_.size)
    
def FromScipy(M):
    """Creates a DOLFIN type matrix from a sparse Scipy matrix
    """
    M = M.tocsr()
    IM = M.indptr
    JM = M.indices
    DM = M.data
    shape = M.shape
    M_ = PETSc.Mat().createAIJWithArrays(shape,(IM,JM,DM))
    return Matrix(PETScMatrix(M_))

def Anorm(A,x):
    """Computes A-norm of x
    || x ||A := x.T * A * x
    """
    tmp = x.copy()
    A.mult(x, tmp)
    return tmp.inner(x)

def cg(A, b, x0=None, ip=None, N=25, tol=1e-8, callback=None):
    """Conjugate Gradient for DOLFIN matrix and vector data types.
    parameters:
        A, ........ lhs-matrix, should be evaluated as a function A(x)
        b, ........ rhs-vector
        x0, ....... initial guess
        ip, ....... inner product for space
        N, ........ max iterations
        tol: ...... residual error tolerance
        callback, . function called at end of each looping
            callback(i,[x0,..],[r0,..],[p0,..])
            parameters:
                i, ........ iteration number
                [x0,..], .. list of solution approximations
                [r0,..], .. list of residuals
                [p0,..], .. list of basis vectors
    """
    # - init -
    zero = zeroVector(len(b))
    tmp = zero.copy()
    xk1 = zero.copy()
    rk1 = zero.copy()
    pk1 = zero.copy()
    Apk = zero.copy()
    # --
    x0 = zero if x0 is None else x0
    ip = eye(len(b)) if ip is None else ip

    A.mult(x0,tmp)
    rk = [b - tmp]
    pk = [rk[-1]]
    xk = [x0]

    for i in range(N):
        # init
        xk1[:] = 0
        rk1[:] = 0
        pk1[:] = 0

        # alpha
        A.mult(pk[-1],Apk)
        ip.mult(rk[-1], tmp)
        a_num = tmp.inner(rk[-1])
        ip.mult(Apk, tmp)
        a_den = tmp.inner(pk[-1])
        ak = a_num/a_den

        # update
        xk1.axpy(  1, xk[-1])
        xk1.axpy( ak, pk[-1])
        rk1.axpy(  1, rk[-1])
        rk1.axpy(-ak, Apk)

        # check tol
        ip.mult(rk1, tmp)
        nrk1 = tmp.inner(rk1)
        if nrk1 < tol:
            break

        # beta
        b_num = nrk1
        b_den = a_num
        bk = b_num/b_den

        # next basis vec
        pk1.axpy( 1, rk1)
        pk1.axpy(bk, pk[-1])

        # save list
        xk.append(xk1.copy())
        rk.append(rk1.copy())
        pk.append(pk1.copy())

        # callback
        callback(i,xk,rk,pk) if callback is not None else None

    return xk1


def cgp(A, P, b, x0=None, ip=None, N=25, tol=1e-8, callback=None, PolakRibiere=False):
    """Preconditioned Conjugate Gradient for DOLFIN matrix and vector data types.
    parameters:
        A, ........ lhs-matrix
        P, ........ preconditioner-matrix
        b, ........ rhs-vector
        x0, ....... initial guess
        ip, ....... inner product for space
        N, ........ max iterations
        tol: ...... residual error tolerance
        callback, . function called at end of each looping
            callback(i,[x0,..],[r0,..],[p0,..])
            parameters:
                i, ........ iteration number
                [x0,..], .. list of solution approximations
                [r0,..], .. list of residuals
                [p0,..], .. list of basis vectors
        PolakRibiere, (default: False) uses the Polak-Ribière formula
                    beta-update. Default is Fletcher-Reeves formula.
    """
    # - init -
    zero = zeroVector(len(b))
    tmp = zero.copy()
    xk1 = zero.copy()
    rk1 = zero.copy()
    zk1 = zero.copy()
    pk1 = zero.copy()
    Apk = zero.copy()
    # --
    x0 = zero if x0 is None else x0
    ip = eye(len(b)) if ip is None else ip
    
    A.mult(x0,tmp)
    rk = [b - tmp]
    P.mult(rk[-1],tmp)
    zk = [tmp.copy()]
    pk = [zk[-1]]
    xk = [x0]
    nr0 = rk[0].inner(rk[0])
    for i in range(N):
        # init
        xk1[:] = 0
        rk1[:] = 0
        zk1[:] = 0
        pk1[:] = 0

        # alpha
        A.mult(pk[-1],Apk)
        ip.mult(rk[-1], tmp)
        a_num = tmp.inner(zk[-1])
        ip.mult(Apk, tmp)
        a_den = tmp.inner(pk[-1])
        ak = a_num/a_den

        # update
        xk1.axpy(  1, xk[-1])
        xk1.axpy( ak, pk[-1])
        rk1.axpy(  1, rk[-1])
        rk1.axpy(-ak, Apk)

        # check tol
        ip.mult(rk1, tmp)
        nrk1 = tmp.inner(rk1)
        if nrk1/nr0 < tol:
            break

        # zk-update
        P.mult(rk1,zk1)
        
        # beta
        ip.mult(rk1-rk[-1],tmp) if PolakRibiere else None
        b_num = tmp.inner(zk1)
        b_den = a_num
        bk = b_num/b_den

        # next basis vec
        pk1.axpy( 1, zk1)
        pk1.axpy(bk, pk[-1])

        # save list
        xk.append(xk1.copy())
        rk.append(rk1.copy())
        zk.append(zk1.copy())
        pk.append(pk1.copy())

        # callback
        callback(i,xk,rk,pk) if callback is not None else None

    return xk1


