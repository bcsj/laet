from numpy import array, argsort, zeros
from numpy import logical_not, logical_or, logical_and
from numpy import int64, float64, NaN

from numba import jit

__all__ = ['glinterp']

@jit(nopython=True)
def index_function(X,xs):
    """
    computes the expression:   array([(X < e).sum()-1 for e in xs])
    but MUCH faster
    """
    N = len(X)
    n = len(xs)
    c = 0
    t = zeros((n,),dtype=int64)
    for k in range(n):
        while X[c] < xs[k]:
            c += 1
            if c >= N: break
        c -= 1
        t[k] = c
    return t

def glinterp_function(x,y,X,Y,Z):
    """
    Linear interpolation function
    Evaluates the gridded data (X,Y,Z) at the points (x,y)
    """
    xi = argsort(x)
    yi = argsort(y)
    xr = argsort(xi)
    yr = argsort(yi)
    xs = x[xi]
    ys = y[yi]

    assert (xs[0] > X[0] and xs[-1] < X[-1]), "Points not contained in grid"
    assert (ys[0] > Y[0] and ys[-1] < Y[-1]), "Points not contained in grid"

    n = len(xs)
    z = zeros((n,),dtype=float64)

    j = index_function(X,xs)
    i = index_function(Y,ys)
    sj = (xs - X[j])/(X[j+1]-X[j])
    si = (ys - Y[i])/(Y[i+1]-Y[i])

    j = j[xr]
    sj = sj[xr]
    i = i[yr]
    si = si[yr]

    n = len(Y)
    z = sj*si*Z[j*n+n+i+1] + sj*(1-si)*Z[j*n+n+i] + (1-sj)*si*Z[j*n+i+1] + (1-sj)*(1-si)*Z[j*n+i]
    return z

def glinterp_function_presort(x,y,xs,ys,xr,yr,X,Y,Z):
    """
    Linear interpolation function with pre-sorting
    Evaluates the gridded data (X,Y,Z) at the points (x,y) but requires the precomputed inputs
        xs = sort(x)
        ys = sort(y)
        xr = argsort(argsort(x)) - indexes for inverting xs to x
        yr = argsort(argsort(y)) - indexes for inverting ys to y
    """
    assert (xs[0] > X[0] and xs[-1] < X[-1]), "Points not contained in grid"
    assert (ys[0] > Y[0] and ys[-1] < Y[-1]), "Points not contained in grid"

    n = len(xs)
    z = zeros((n,),dtype=float64)

    j = index_function(X,xs)
    i = index_function(Y,ys)
    sj = (xs - X[j])/(X[j+1]-X[j])
    si = (ys - Y[i])/(Y[i+1]-Y[i])

    j = j[xr]
    sj = sj[xr]
    i = i[yr]
    si = si[yr]

    n = len(Y)
    z = sj*si*Z[j*n+n+i+1] + sj*(1-si)*Z[j*n+n+i] + (1-sj)*si*Z[j*n+i+1] + (1-sj)*(1-si)*Z[j*n+i]
    return z

def glinterp(X,Y,Z,presort=False):
    """ Returns a linear grid interpolator
    >> f = glinterp(X,Y,Z)
    Parameters:
        X, Y:   Should be 1d-arrays with the xy values of the grid points. Must be
                ordered; e.g. X[0] < X[1] < ... < X[n] and likewise for Y.
        Z:      Should be a 2d-array with z values for the grid points, such that
                Z[i,j] corresponds to the grid point (X[i],Y[j]).
    Output:
        f:      Function f(x,y) which evaluates the interpolation. Supports vector inputs.
    """
    X = array(X)
    Y = array(Y)
    Z = array(Z)
    ZZ = Z.ravel()
    
    if presort:
        f = lambda x,y,xs,ys,xr,yr: glinterp_function_presort(x,y,xs,ys,xr,yr,X,Y,ZZ)
    else:
        f = lambda x,y: glinterp_function(x,y,X,Y,ZZ)
    
    return f
