from mshr import generate_mesh
from mshr import Circle
from dolfin import Point
from dolfin import Mesh

__all__ = [
    'Load',
    'UnitCircleMesh',
    'DiscMesh']

def Load(fn):
    return Mesh(fn)

def UnitCircleMesh(n):
    C = Circle(Point(0,0),1)
    return generate_mesh(C,n)

def DiscMesh(n, radius=1):
    C = Circle(Point(0,0), radius)
    return generate_mesh(C,n)
