from dolfin import __version__ as DOLFINversion

from dolfin import TestFunction, assemble, ds
from dolfin import UserExpression

from numpy import sqrt

__all__ = ['std']

def std(V, scale=1.0):
    """ Standard boundary conditions
    Returns DOLFIN vectors corresponding to the
    standard boundary conditions: x, y, (x+y)/sqrt(2)
    """
    class expr(UserExpression):
        def __init__(self, **kwargs):
            self.f = kwargs.pop('f')
            None if DOLFINversion == '2017.2.0' \
                else super(expr,self).__init__(**kwargs)
        def eval(self, value, x):
            value[0] = self.f(*x)
        def value_shape(self):
            return ()

    c = sqrt(2)
    F = [expr(f=lambda x,y: scale * x, degree=1),
        expr(f=lambda x,y: scale * y, degree=1),
        expr(f=lambda x,y: scale * (x+y)/c, degree=1)]

    (phi,_) = TestFunction(V)
    return [assemble(f*phi*ds) for f in F]

