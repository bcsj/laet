from dolfin import Function
from numpy import array, linspace, sqrt
from numpy import logical_not, logical_or, logical_and

__all__ = ['mix','disc','headmodel']

def ToVector(fn):
    """
    decorator method
    """
    def wrapper(Vs, *args, **kwargs):
        z = Function(Vs).vector()
        z[:] = fn(Vs, *args, **kwargs)
        return z
    return wrapper

@ToVector
def mix(Vs):
    """
    Forms mix-conductivity phantom.
    """
    xy = Vs.tabulate_dof_coordinates().reshape((-1,2))
    from numpy import cos, sin, exp, pi
    from matplotlib.path import Path
    
    def kite(x0=0,y0=0,scale=1,theta=0,n=50):
        R = array([[cos(theta),sin(theta)],[-sin(theta),cos(theta)]])
        S = array([[scale,0],[0,scale]])
        t = linspace(0, 2*pi, n)
        x = cos(t) + 0.65*cos(2*t) - 0.65
        y = 1.5*sin(t)
        xy = array([x,y])
        xy = S.dot(xy)
        xy = R.dot(xy)
        x,y = xy[0,:]+x0,xy[1,:]+y0
        return x,y
   
    # Background
    z = 0.1 + 1.4*exp( -( (xy[:,0] - 0.35)**2 + (xy[:,1] - 0.25)**2 ) / 0.025 )
    
    # Kite shape
    x,y = kite(x0=-0.4, scale=0.2)
    p = Path([(x[i],y[i]) for i in range(len(x)) ])
    index = p.contains_points(xy)
    z[index] = 1.0

    # Box shape
    logical_and(0.15 < xy[:,0],xy[:,0] < 0.35,out=index)
    logical_and(index,-0.6 < xy[:,1],out=index)
    logical_and(index,xy[:,1] < -0.4,out=index)
    z[index] = 0.5
    
    return z

@ToVector
def disc(Vs,center=(0,0),radius=.5,bg=1.0,disc=1.5):
    """ Domain with background and a disc inclusion.
    Parameters:
        xy, the coordinates to evaluate at
        center, [default: (0,0)] center of disc
        radius, [default: 0.5] radius of disc
        bg, [default: 1.0] background conductivity
        disc, [default: 1.5] disc conductivity
    """
    xy = Vs.tabulate_dof_coordinates().reshape((-1,2))
    z = bg + 0*xy[:,0]
    idx = (xy[:,0]-center[0])**2 + (xy[:,1]-center[1])**2 <= radius**2
    z[idx] = disc
    return z

def package_dir():
    from os.path import dirname, realpath
    return dirname(realpath(__file__))

@ToVector
def headmodel(Vs):
    """
    Forms a conductivity phantom from the image file "headmodel.png" found
    in the package directory.
    """
    xy = Vs.tabulate_dof_coordinates().reshape((-1,2))
    from PIL import Image
    from os.path import join
    from numpy import rot90

    color2cond = {
        (255,255,255): 0.4,     # air
        (  0,255,  0): 0.5232,  # scalp
        (  0,  0,255): 0.2983,  # skull
        (255,  0,  0): 1.0143,  # spinal fluid
        (255,255,  0): 0.55946, # gray matter
        (  0,255,255): 0.324040,# white matter
    }

    imfile = join(package_dir(),'headmodel.png')
    im = Image.open(imfile)
    arr = array([color2cond[pixel[0:3]] for pixel in list(im.getdata())])
    arr.shape = im.size
    
    N,M = im.size
    Im = rot90(arr,3)
    j = (N-1)*(xy[:,0]+1.0)/2.0
    i = (M-1)*(xy[:,1]+1.0)/2.0
    Ij = j.astype(int)
    Ii = i.astype(int)
    idxj = logical_not(logical_or(logical_and(Ij == 0, j < 0), Ij == N-1))
    idxi = logical_not(logical_or(logical_and(Ii == 0, i < 0), Ii == M-1))
    Inj = Ij.copy()
    Ini = Ii.copy()
    Inj[idxj] = Inj[idxj] + 1
    Ini[idxi] = Ini[idxi] + 1
    sj = j - Ij
    si = i - Ii
    z = sj*si*Im[Inj,Ini] + sj*(1-si)*Im[Inj,Ii] + (1-sj)*si*Im[Ij,Ini] + (1-sj)*(1-si)*Im[Ij,Ii]
    
    return z
