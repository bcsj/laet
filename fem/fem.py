from dolfin import __version__ as DOLFINversion

from dolfin import FiniteElement, MixedElement, triangle
from dolfin import FunctionSpace, VectorFunctionSpace
from dolfin import TrialFunction, TestFunction, Function
from dolfin import inner, dot, grad, dx, ds
from dolfin import sqrt as dlfsqrt
from dolfin import assemble

from dolfin import PETScLUSolver, as_backend_type
from numpy import append

__all__ = ['V',
           'Vs',
           'Vq',
           'VsVInjector',
           'norm2',
           'normp',
           'TVseminorm',
           'MassMatrixV',
           'StiffnessMatrixV',
           'MixedMatrixV',
           'MassMatrixVs',
           'StiffnessMatrixVs',
           'MixedMatrixVs',
           'GradientSolver',
           'Solver',
           'Vector']

def V(mesh, degree=1):
    E1 = FiniteElement('P', triangle, degree)
    E0 = FiniteElement('R', triangle, 0)

    return FunctionSpace(mesh, MixedElement(E1,E0))

def Vs(mesh, degree=1):
    E1 = FiniteElement('P', triangle, degree)
    return FunctionSpace(mesh, E1)

def Vq(mesh, degree=1):
    return VectorFunctionSpace(mesh, 'CG', degree)

def VsVInjector(Vs,V):
    from petsc4py import PETSc
    from dolfin import Matrix, PETScMatrix
    n = Vs.dim()
    pos = V.sub(1).dofmap().dofs()[0]
    if pos == 0:
        P = PETSc.Mat().createAIJWithArrays((n+1,n), \
              ([0]+list(range(n+1)),list(range(n)),[1 for _ in range(n)]))
    elif pos == n:
        P = PETSc.Mat().createAIJWithArrays((n+1,n), \
              (list(range(n+1))+[n],list(range(n)),[1 for _ in range(n)]))
    else:
        raise Exception('Constraint subspace not found')
    return Matrix(PETScMatrix(P))

def norm2(v,M):
    tmp = v.copy()
    M.mult(v,tmp)
    return tmp.inner(v)

def normp(v,V,p,quadrature_degree=None):
    u = Function(V,v)
    dX = dx if quadrature_degree is None \
            else dx(metadata={'qudrature_degree':quadrature_degree})
    v = assemble(dlfsqrt(u*u)**p*dX)**(1.0/p)
    return v

def TVseminorm(v,V):
    u = Function(V,v)
    return assemble(dlfsqrt(dot(grad(u),grad(u)))*dx)

def MassMatrixV(V,w=None):
    (u,c) = TrialFunction(V)
    (v,d) = TestFunction(V)

    a = u*v
    if w is not None: a *= w
    a *= dx
    a += u*d*ds + v*c*ds

    return assemble(a)

def BdryMassMatrixV(V,w=None):
    (u,c) = TrialFunction(V)
    (v,d) = TestFunction(V)
    
    a = u*v
    if w is not None: a *= w
    a *= ds
    a += u*d*ds + v*c*ds

    return assemble(a)
    

def StiffnessMatrixV(V,w=None):
    (u,c) = TrialFunction(V)
    (v,d) = TestFunction(V)

    a = dot(grad(u),grad(v))
    if w is not None: a *= w
    a *= dx
    a += u*d*ds + v*c*ds

    return assemble(a)

def MixedMatrixV(V,f,w=None):
    (u,c) = TrialFunction(V)
    (v,d) = TestFunction(V)

    a = u*dot(grad(f),grad(v))
    if w is not None: a *= w
    a *= dx
    a += u*d*ds + v*c*ds

    return assemble(a)

def MassMatrixVs(Vs,w=None):
    u = TrialFunction(Vs)
    v = TestFunction(Vs)

    a = u*v
    if w is not None: a *= w
    a *= dx

    return assemble(a)

def StiffnessMatrixVs(Vs,w=None):
    u = TrialFunction(Vs)
    v = TestFunction(Vs)

    a = dot(grad(u),grad(v))
    if w is not None: a *= w
    a *= dx

    return assemble(a)

def MixedMatrixVs(Vs,f,w=None):
    u = TrialFunction(Vs)
    v = TestFunction(Vs)

    a = u*dot(grad(f),grad(v))
    if w is not None: a *= w
    a *= dx

    return assemble(a)

def GradientSolver(Vq,Vs,fastapprox=False):
    """
    Based on:
        https://fenicsproject.org/qa/1425/derivatives-at-the-quadrature-points/
    """
    uq = TrialFunction(Vq)
    vq = TestFunction(Vq)
    M = assemble(inner(uq,vq)*dx)
    if fastapprox:
        # Assumes the mass matrix is diagonal (or very close to)
        ones = Function(Vq)
        ones.vector()[:] = 1
        Md = M*ones.vector()
    else:
        # Full FEM-solve
        femSolver = Solver(M)
    u = TrialFunction(Vs)
    P = assemble(inner(vq,grad(u))*dx)

    def GradSolver(uvec):
        g = P*uvec
        if fastapprox:
            gv = g.get_local()/Md.get_local()
        else:
            gv = Vector(Vq)
            femSolver.solve(gv, g)
        dx = Vector(Vs)
        dy = Vector(Vs)
        dx[:] = gv[0::2].copy()
        dy[:] = gv[1::2].copy()
        return dx,dy

    return GradSolver

def Solver(Op):
    s = PETScLUSolver(as_backend_type(Op),'mumps')
    return s

def Vector(V):
    return Function(V).vector()


